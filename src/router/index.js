import Vue from 'vue';
import Router from 'vue-router';
import Index from '@/components/Index';
import Register from '@/components/Register';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/register',
      name: 'SigningUp',
      component: Register,
    },
    {
      path: '/',
      name: 'IndexPage',
      component: Index,
    },
  ],
});
