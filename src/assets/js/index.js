export default {
  name: 'IndexPage',
  data() {
    return {
      title: 'Спортивное Подмосковье',
      slogan: '«Массовый спорт — это фундамент спортивных побед.»',
      name: 'Владимир Путин',
    };
  },
};
